package net.nullpointer.panel;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JPanel;

import org.apache.commons.lang3.RandomUtils;

import net.nullpointer.entity.Card;
import net.nullpointer.main.Memory;
import net.nullpointer.main.Util;

public class GameGrid extends JPanel {
	
	/**
	 * 
	 */
	
	//TODO: Generate number ID
	private static final long serialVersionUID = 1L;

	int size;
	
	ArrayList<Card> cards;
	ArrayList<Integer> usedIds;
	
	// Test
	static public void main(String... args){
		System.out.println(Util.findNextSquare(50));
	}
	
	public GameGrid(int pairs){
		size = Util.findNextSquare(pairs);
		this.setSize(550, 550);
		setLayout(new GridLayout(size, size, 5, 5));
		
		cards = new ArrayList<Card>();
		usedIds = new ArrayList<Integer>();
		
		for (int i = 0; i < pairs*2; i+=2){
			// Generate random non-repeated id
			int randId = 1;
			while(usedIds.contains(randId)){
				System.out.println("Duplicate id found");
				randId = RandomUtils.nextInt(1, 50);
			}
			usedIds.add(randId);
			
			Card card1 = new Card(randId);
			card1.addActionListener(new CardSelection());
			Card card2  = new Card(randId);
			card2.addActionListener(new CardSelection());
			
			cards.add(card1); cards.add(card2);

		}
		
		// Randomize order and add
		Collections.shuffle(cards);
		for(Card c : cards)
			add(c);
		
		
		// Fill the rest of space
		while(cards.size() < size*size){
			Card card = new Card(0);
			add(card);
			cards.add(card);
		}
	}
	
	public void hideAll(){
		for(Card c : cards){
			c.hide();
		}
	}
	
	private class CardSelection implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// Add this class to the selected
			if(Memory.selectedCards.size() < 2 && !Memory.selectedCards.contains( (Card)e.getSource()) ){
				Memory.selectedCards.add( (Card)e.getSource() );
				((Card)e.getSource()).show();
			}
		}
		
	}

}
