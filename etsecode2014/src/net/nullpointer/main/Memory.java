package net.nullpointer.main;

import java.util.ArrayList;

import net.nullpointer.entity.Card;
import net.nullpointer.frames.GameFrame;
import net.nullpointer.frames.MainMenuFrame;

public class Memory {

	// JFrames
	public static MainMenuFrame menuFrame = new MainMenuFrame();
	public static GameFrame gameFrame = new GameFrame();
	
	// The current turn
	public static int playerTurn = 0;
	
	// Player Names
	public static String player1 = "default";
	public static String player2 = "default2";
	// Scores
	public static int score[] = {0, 0};

	public static int pairs = 10;
	
	// Contains the last two selected cards
	public static ArrayList<Card> selectedCards;
	public static boolean gameisActive = false;
	
	/** Program entry point	 */
	public static void main(String[] args){
		new Memory();
	}
	
	public Memory(){
		
		selectedCards = new ArrayList<Card>();
		
		menuFrame.setVisible(true);
		//gameFrame.setVisible(true);
		
		
	}
}
