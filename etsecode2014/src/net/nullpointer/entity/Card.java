package net.nullpointer.entity;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Card extends JButton{

	/*Image icon for the button */
	private Icon icon;
	BufferedImage img;
	public boolean locked = false;
	
	/** Unique id to check for match */
	public int id;
	
	public Card(int id){
		
		if(id == 0){
			this.icon = null;
			return;
		}
		
		
		// Try to find the image
		try { img = ImageIO.read(new File("img"+File.separator+id+".jpg")); }
		catch (IOException e) { e.printStackTrace(); }
		
		this.id = id;
		this.icon = new ImageIcon(img);
		this.setIcon(icon);
	}
	
	public void show(){
		setIcon(icon);
	}
	
	public void hide(){
		if(!locked)
			setIcon(null);
	}
	
//	public boolean equals(Object o){
//		return (this.id == ((Card)o).id);
//	}
	
	
}
