package net.nullpointer.frames;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.nullpointer.entity.Card;
import net.nullpointer.main.Memory;
import net.nullpointer.panel.GameGrid;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class GameFrame extends JFrame {
	private JTextField textScoreP1Field;
	private JTextField textScoreP2Field;

	JLabel lblPlayer1;
	JLabel lblPlayer2;
	JLabel lblPlayerTrun;

	JButton btnStartGame;

	// The update thread
	Thread uThread;

	GameGrid gameGrid;

	public GameFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setSize(960, 600);
		setTitle("Memory - Play Arena");
		getContentPane().setLayout(null);

		generateContents();
	}

	public void generateContents() {

		JLabel labelSideTitle = new JLabel("Memory Card Game");
		labelSideTitle.setFont(new Font("Tahoma", Font.BOLD, 16));
		labelSideTitle.setBounds(570, 11, 216, 20);

		// Sidebar

		getContentPane().add(labelSideTitle);

		JLabel lblScoreboard = new JLabel("Scoreboard:");
		lblScoreboard.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblScoreboard.setBounds(570, 42, 141, 20);
		getContentPane().add(lblScoreboard);

		lblPlayer1 = new JLabel("Player 1:");
		lblPlayer1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPlayer1.setBounds(580, 73, 160, 20);
		getContentPane().add(lblPlayer1);

		lblPlayer2 = new JLabel("Player 1:");
		lblPlayer2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPlayer2.setBounds(580, 104, 160, 20);
		getContentPane().add(lblPlayer2);

		textScoreP1Field = new JTextField();
		textScoreP1Field.setHorizontalAlignment(SwingConstants.CENTER);
		textScoreP1Field.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textScoreP1Field.setText("0");
		textScoreP1Field.setEditable(false);
		textScoreP1Field.setBounds(750, 73, 36, 22);
		getContentPane().add(textScoreP1Field);
		textScoreP1Field.setColumns(10);

		textScoreP2Field = new JTextField();
		textScoreP2Field.setHorizontalAlignment(SwingConstants.CENTER);
		textScoreP2Field.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textScoreP2Field.setText("0");
		textScoreP2Field.setEditable(false);
		textScoreP2Field.setColumns(10);
		textScoreP2Field.setBounds(750, 104, 36, 22);
		getContentPane().add(textScoreP2Field);

		lblPlayerTrun = new JLabel("Player PLAYER has the turn");
		lblPlayerTrun.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlayerTrun.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPlayerTrun.setBounds(570, 231, 374, 20);
		lblPlayerTrun.setVisible(false);
		getContentPane().add(lblPlayerTrun);

		btnStartGame = new JButton("Start the Game");
		btnStartGame.addActionListener(new StartGameAction());
		btnStartGame.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnStartGame.setBounds(671, 262, 160, 44);
		getContentPane().add(btnStartGame);
	}

	public void refreshDisplay() {
		lblPlayer1.setText(Memory.player1 + ":");
		lblPlayer2.setText(Memory.player2 + ":");

		textScoreP1Field.setText("" + Memory.score[0]);
		textScoreP2Field.setText("" + Memory.score[1]);

		String playerTurn = "Player "
				+ (Memory.playerTurn == 0 ? Memory.player1 : Memory.player2)
				+ "'s Turn.";
		lblPlayerTrun.setText(playerTurn);

	}

	public void refreshData() {
		initGrid();
		refreshDisplay();
	}

	/** Initialize grid and set up the pair matrix */
	public void initGrid() {
		// Try to remove any previous game grids
		try {
			getContentPane().remove(gameGrid);
		} catch (Exception e) {
		}

		// Generate a new one
		gameGrid = new GameGrid(Memory.pairs);
		gameGrid.setBounds(10, 11, 550, 550);
		getContentPane().add(gameGrid);
	}

	private class StartGameAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			btnStartGame.setEnabled(false);
			lblPlayerTrun.setVisible(true);

			// Set the active flag
			Memory.gameisActive = true;

			gameGrid.hideAll();

			uThread = new Thread(new Runnable() {
				@Override
				public void run() {
					while (Memory.gameisActive) {

						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
						refreshDisplay();
						
						// Do things if there are two selected cards
						if (Memory.selectedCards.size() == 2) {
							
							if (Memory.selectedCards.get(0).id == Memory.selectedCards.get(1).id) {
								System.out.println("Player "+Memory.playerTurn+" has scored" );
								for (Card c : Memory.selectedCards)
									c.locked = true;
								Memory.score[Memory.playerTurn] += 1;
							} else {
								Memory.playerTurn = Memory.playerTurn == 1 ? 0 : 1;
							}

							gameGrid.hideAll();
							Memory.selectedCards.clear();
						}

					}
				}
			});
			uThread.start();
		}

	}
}
