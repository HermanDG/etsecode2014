package net.nullpointer.frames;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import net.nullpointer.main.Memory;

@SuppressWarnings("serial")
public class MainMenuFrame extends JFrame {
	private JTextField textJugador1Field;
	private JTextField textJugador2Field;
	private JTextField textTamañoField;
	public MainMenuFrame() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		
		setTitle("Memory - Main Menu");
		setSize(500, 400);
		getContentPane().setLayout(null);
		
		textJugador1Field = new JTextField();
		textJugador1Field.addKeyListener(new CheckName(textJugador1Field));
		textJugador1Field.setBounds(249, 136, 175, 20);
		getContentPane().add(textJugador1Field);
		textJugador1Field.setColumns(10);
		
		JLabel lblMemory = new JLabel("MEMORY");
		lblMemory.setBounds(159, 34, 160, 61);
		getContentPane().add(lblMemory);
		lblMemory.setFont(new Font("Tahoma", Font.PLAIN, 40));
		
		JLabel lblJugador = new JLabel("Jugador 1");
		lblJugador.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblJugador.setBounds(89, 131, 86, 23);
		getContentPane().add(lblJugador);
		
		JLabel lblJugador_1 = new JLabel("Jugador 2");
		lblJugador_1.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblJugador_1.setBounds(89, 165, 86, 23);
		getContentPane().add(lblJugador_1);
		
		textJugador2Field = new JTextField();
		textJugador2Field.addKeyListener(new CheckName(textJugador2Field));
		textJugador2Field.setColumns(10);
		textJugador2Field.setBounds(249, 170, 175, 20);
		getContentPane().add(textJugador2Field);
		
		JLabel lblTamao = new JLabel("Tama\u00F1o");
		lblTamao.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblTamao.setBounds(107, 221, 68, 20);
		getContentPane().add(lblTamao);
		
		textTamañoField = new JTextField();
		textTamañoField.setBounds(249, 221, 36, 20);
		getContentPane().add(textTamañoField);
		textTamañoField.setColumns(10);
		
		JLabel lblParejas = new JLabel("parejas");
		lblParejas.setBounds(302, 224, 46, 14);
		getContentPane().add(lblParejas);
		
		JButton btnEmpezar = new JButton("Empezar");
		btnEmpezar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(!validate(textJugador1Field) || !validate(textJugador2Field) || !validate(textTamañoField))
					return;
				
				if (Integer.parseInt(textTamañoField.getText()) < 3 || Integer.parseInt(textTamañoField.getText()) > 50) {
					JOptionPane.showMessageDialog(Memory.menuFrame, "El tamaño debe estar entre 3 y 50 parejas", "Advertencia", JOptionPane.OK_OPTION);
					return;
				}
						
				Memory.player1 = textJugador1Field.getText();
				Memory.player2 = textJugador2Field.getText();
				Memory.pairs = Integer.parseInt(textTamañoField.getText());
				
				Memory.gameFrame.refreshData();
				Memory.menuFrame.setVisible(false);
				Memory.gameFrame.setVisible(true);
				
				}
		});
		btnEmpezar.setBounds(148, 290, 200, 50);
		getContentPane().add(btnEmpezar);
	}
		
		public boolean validate(JTextField field) {
			
			if (field.getText().length() == 0) {
				JOptionPane.showMessageDialog(Memory.menuFrame, "Rellena todos los campos", "Advertencia", JOptionPane.OK_OPTION);
				return false;
		}
			return true;
		
	}
	
	private class CheckName extends KeyAdapter{
		
		JTextField field;
		public CheckName(JTextField field){
			this.field = field;
		}
		
		public void keyTyped(KeyEvent arg0) {
			if(field.getText().length()>=16) { 
			JOptionPane.showMessageDialog(Memory.menuFrame, "El nombre no puede superar los 16 caracteres", "Advertencia", JOptionPane.OK_OPTION);
			String newName = field.getText().substring(0, 15);
			field.setText(newName);
			}
		}
	}
}
